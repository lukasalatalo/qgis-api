package main

import (
	"database/sql"
	"flag"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"strconv"
	"strings"
)

var (
	QGISDB       *sql.DB
	QGISURI      = flag.String("qgissql", "host=qgis.dc1.osh.telmax.ca port=5432 user=qgis password=qgis dbname=design_archive sslmode=disable", "PGSQL connection string for QGIS DB")
	QGISDatabase = flag.String("qgisdb", "design_archive", "The QGIS database to look in for demand points")
	QueryFields  = `select id, da_name, wirecentre, full_name, civic_num, unit_num, streetname, town, municipali, province, postal_code, bldg_id, is_mdu, lead_id, customer_id, status, size, fdh_id, fdh_port, terminal_id, terminal_port, pon, init_signal, con_schedule, ST_X(ST_Transform(ST_GeometryN(geom, 1), 4326)), ST_Y(ST_Transform(ST_GeometryN(geom,1),4326)),access_type from public.demand_points`
)

func init() {
	flag.Parse()
	var err error
	QGISDB, err = sql.Open("postgres", *QGISURI)
	if err != nil {
		log.Errorf("Could not connect to QGIS Postgres %v", err)
	} else {
		err = QGISDB.Ping()
		if err != nil {
			log.Errorf("Could not ping QGIS Postgres %v", err)
		} else {
			log.Info("Connection to QGIS database successful!")
		}
	}
}

func GetSite(id int) (site Site, err error) {
	log.Infof("Getting site %v from demand points", id)
	var rows *sql.Rows
	rows, err = QGISDB.Query(QueryFields+` where id=$1`, id)

	if err != nil {
		log.Errorf("Problem getting site %v from QGIS - error %v", site, err)
	} else {
		rows.Next()
		site, err = ProcessRow(rows)
	}
	//	rows.Close()
	return
}

func GetAddress(address string) (sites []Site, err error) {
	log.Infof("Getting sites matching %v from demand points", address)
	var rows *sql.Rows

	rows, err = QGISDB.Query(QueryFields+` where full_name like $1 || '%' limit 20`, address)
	defer rows.Close()
	if err != nil {
		log.Errorf("Problem getting site %v from QGIS - error %v", address, err)
	} else {
		for rows.Next() {
			var site Site
			site, err = ProcessRow(rows)
			sites = append(sites, site)
		}
	}
	return

}

func SearchSites(filters []Filter, limit int) (sites []Site, err error) {
	log.Infof("Getting sites matching  %v from demand points", filters)
	var rows *sql.Rows
	var filter_params []string
	if limit == 0 {
		limit = 20
	}
	query := QueryFields + ` where `

	for _, filter := range filters {
		filter_params = append(filter_params, filter.Key+"='"+filter.Value.(string)+"'")
	}
	parameters := strings.Join(filter_params, ` AND `)
	rows, err = QGISDB.Query(query+parameters+` limit $1`, limit)
	defer rows.Close()
	if err != nil {
		log.Errorf("Problem getting site %v from QGIS - error %v", query+parameters+` limit 20`, err)
	} else {
		for rows.Next() {
			var site Site
			site, err = ProcessRow(rows)
			sites = append(sites, site)
		}
	}
	return
}

func DropSites(da int) (sites []Site, err error) {
	log.Infof("Getting sites needing drops for DA %v from demand points", da)
	var rows *sql.Rows
	query := QueryFields + ` where `

	rows, err = QGISDB.Query(query+` da=$1 AND lead_id is not null AND lead_id != '' and status !='Serviced' AND status != 'Installed' order by streetname, NULLIF(regexp_replace(civic_num, '\D', '', 'g'), '')::int `, da)
	defer rows.Close()
	if err != nil {
		log.Errorf("Problem getting sites for DA %v from QGIS - error %v", da, err)
	} else {
		for rows.Next() {
			var site Site
			site, err = ProcessRow(rows)
			sites = append(sites, site)
		}
	}
	return
}

func GetFDHList(da int) (fdhlist []string, err error) {
	log.Infof("Getting FDH list for DA %v", da)
	var rows *sql.Rows
	query := `select fdh_id from public.demand_points where da=$1 group by fdh_id order by fdh_id`

	rows, err = QGISDB.Query(query, da)
	defer rows.Close()
	if err != nil {
		log.Errorf("Problem getting FDH list for DA %v from QGIS - error %v", da, err)
	} else {
		for rows.Next() {
			var fdh *string
			err = rows.Scan(&fdh)
			if fdh != nil {
				fdhlist = append(fdhlist, *fdh)
			}
		}
	}
	return
}

func GetDAList() (dalist []int, err error) {

	var rows *sql.Rows
	query := `select da from public.demand_points group by da order by da`

	rows, err = QGISDB.Query(query)
	defer rows.Close()
	if err != nil {
		log.Errorf("Problem getting DA list from QGIS - error %v", err)
	} else {
		for rows.Next() {
			var da *int
			err = rows.Scan(&da)
			if da != nil {
				dalist = append(dalist, *da)
			}
		}
	}
	return
}

func ProcessRow(rows *sql.Rows) (site Site, err error) {
	//	var da int
	var fdh *string
	var fdh_port *string
	var terminal *string
	var terminalport *int64
	var pon *string
	var size *int64
	var wirecentre *string
	var town *string
	var is_mdu *string
	var circuitdata Circuit
	var location GeoPoint
	var accesstype *string
	var status *string
	var signal *float64
	//	var postalcode *sql.NullString
	err = rows.Scan(&site.ID, &site.DA, &wirecentre, &site.FullAddress, &site.Civic_num, &site.Unit_num, &site.Streetname, &town, &site.Municipality, &site.Province, &site.PostalCode, &site.Bldg_id, &is_mdu, &site.Lead_ID, &site.AccountCode, &status, &size, &fdh, &fdh_port, &terminal, &terminalport, &pon, &signal, &site.ConstructionSchedule, &location.Long, &location.Lat, &accesstype)
	/*
		if postalcode.Valid {
			*site.PostalCode = postalcode.String
		} else {
			site.PostalCode = nil
		}
	*/
	//	site.DA = strconv.Itoa(da)
	site.Is_mdu = CheckNullBool(is_mdu)
	site.Town = CheckNullString(town)
	site.WireCentre = CheckNullString(wirecentre)
	site.FSDA = CheckNullString(fdh)
	circuitdata.FDH = CheckNullString(fdh)
	fdhportint, _ := strconv.ParseInt(CheckNullString(fdh_port), 10, 32)
	circuitdata.FDHPort = int(fdhportint)
	circuitdata.Terminal = CheckNullString(terminal)
	circuitdata.TerminalPort = CheckNullInt(terminalport)
	circuitdata.PON = CheckNullString(pon)
	circuitdata.Fibre = 1
	circuitdata.DemarcPort = 1
	circuitdata.InitialSignal = CheckNullFloat(signal)
	site.FibreSize = CheckNullInt(size)
	if site.FibreSize == -1 {
		site.FibreSize = 2
	}
	if status == nil {
		site.Status = "Not Serviced"
	} else {
		site.Status = *status
	}
	site.Location = location

	if site.Status == "Planned" || site.Status == "Construction" || site.Status == "Drop Placed" || site.Status == "Drop Construction" {
		site.AccessType = "FibreBuild"
		site.CircuitData = append(site.CircuitData, circuitdata)

	} else if site.Status == "Serviced" || site.Status == "Installed" {
		site.AccessType = "Fibre"
		site.CircuitData = append(site.CircuitData, circuitdata)

	} else {
		if accesstype == nil {
			site.AccessType = "Not Serviced"
		} else if *accesstype == "TPIA" {
			site.AccessType = "TPIA"
		} else {
			site.AccessType = "Not Serviced"
		}
	}

	return
}

func CheckTPIA(point GeoPoint) (tpia_zone string, err error) {
	// Dummy check for now
	if point.Long < -78.65576 && point.Long > -79.15709 && point.Lat > 43.789301 && point.Lat < 43.976560 {
		tpia_zone = "Oshawa"
		log.Info("TPIA check qualifies")
	}
	return
}

func CheckNullString(input *string) string {
	if input == nil {
		return ""
	}
	return *input
}

func CheckNullInt(input *int64) int {
	if input == nil {
		return 0
	}
	return int(*input)
}

func CheckNullFloat(input *float64) float64 {
	if input == nil {
		return 0
	}
	return float64(*input)
}

func CheckNullBool(input *string) bool {
	if input == nil {
		return false
	} else {
		if *input == "true" || *input == "TRUE" {
			return true
		}
	}
	return false
}

func QGISDBCleanup() {
	QGISDB.Close()
}

func FixNoPostalcode(count int) {
	log.Info("Getting sites with missing postal codes")

	rows, err := QGISDB.Query(`select id, full_name from public.demand_points where postal_code is null and civic_num is not null limit $1`, count)
	if err != nil {
		log.Errorf("Problem with postal code query %v", err)
	} else {
		var updaterows *sql.Rows
		for rows.Next() {
			var id string
			var fulladdress string
			err = rows.Scan(&id, &fulladdress)
			if err != nil {
				log.Errorf("Problem processing row %v", err)
			} else {
				log.Debugf("Geocoding %v", fulladdress)
				georesult, err := Geocode(fulladdress)
				if err != nil {
					log.Errorf("Problem geocoding address %v %v", fulladdress, err)
				} else {
					var site Site
					GeoResultToSite(georesult, &site)
					if site.PostalCode != nil {
						postalcode := *site.PostalCode
						log.Infof("Updating postal code for site %v to %v", fulladdress, postalcode)
						updaterows, err = QGISDB.Query(`update public.demand_points set postal_code=$1 where id=$2`, postalcode, id)
						updaterows.Close()
						if err != nil {
							log.Errorf("Problem updating postal code for ID %v address %v postal code %v", id, fulladdress, postalcode)
						}
					} else {
						log.Errorf("Didn't get a postal code for %v", fulladdress)
					}
				}
			}
		}
	}
}

func UpdateSiteLead(id int64, lead string) error {
	query := "update public.demand_points set lead_id = $1 where id = $2"
	rows, err := QGISDB.Query(query, lead, id)
	if err != nil {
		log.Errorf("Problem updating lead on site ID %v", id, err)
		return err
	} else {
		log.Debugf("Result from update query is %v", rows)
	}
	return nil
}

func UpdateSiteAccount(id int64, account string) error {
	query := "update public.demand_points set customer_id = $1 where id = $2"
	rows, err := QGISDB.Query(query, account, id)
	if err != nil {
		log.Errorf("Problem updating account on site ID %v", id, err)
		return err
	} else {
		log.Debugf("Result from update query is %v", rows)
	}
	return nil
}

type Filter struct {
	Key   string
	Value interface{}
}

func UpdateSiteStatus(id int64, status string) error {
	var query string
	if status == "Serviced" || status == "Installed" {
		query = "update public.demand_points set status = $1, access_type = 'Fibre' where id = $2"
	} else {
		query = "update public.demand_points set status = $1 where id = $2"
	}
	rows, err := QGISDB.Query(query, status, id)
	if err != nil {
		log.Errorf("Problem updating account on site ID %v", id, err)
		return err
	} else {
		log.Debugf("Result from update query is %v", rows)
	}
	return nil
}

func UpdateSiteTest(id int64, pon string, fdhport int64, terminal string, termport int64, signal float64) error {
	query := "update public.demand_points set pon = $1, access_port = $1, terminal_id = $2, init_signal = $3, fdh_port = $4, terminal_port = $5, date_tested=now() where id = $6"
	log.Infof("Query is %v", query)
	rows, err := QGISDB.Query(query, pon, terminal, signal, fdhport, termport, id)
	if err != nil {
		log.Errorf("Problem updating account on site ID %v", id, err)
		return err
	} else {
		log.Debugf("Result from update query is %v", rows)
		query = "update public.demand_points set status = 'Serviced' where status != 'Installed' AND id = $1"
		_, err = QGISDB.Query(query, id)
		if err != nil {
			log.Errorf("Problem updating account on site ID %v", id, err)
			return err
		}
	}
	return nil
}
