package main

import (
	"encoding/json"
	"errors"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"net/url"
)

var (
	GeoAPIKey = "AIzaSyAN0quOvfg_RAn9q1AK3ivR2tsQChKXa5g"
	GeoAPIURL = "https://maps.googleapis.com/maps/api/geocode/json"
)

func Geocode(address string) (result GeoResult, err error) {
	address = url.QueryEscape(address + " Canada")
	query := GeoAPIURL + "?address=" + address + "&region=ca&key=" + GeoAPIKey

	resp, err := http.Get(query)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	var Result struct {
		Status  string      `json:"status"`
		Results []GeoResult `json:"results"`
		Error   string      `json:"error_message"`
	}
	err = json.Unmarshal(body, &Result)
	if err != nil {
		log.Errorf("Problem with Geocoding API - address is %v, error is %v", address, err)
	} else {
		if Result.Status == "OK" {
			result = Result.Results[0]
			log.Infof("Geocoding Successful")
			log.Debugf("Geocoding result %v", result)
		} else {
			err = errors.New(Result.Error)
		}
		log.Debugf("Geocode result is %v", Result)
	}
	return
}

func GeoResultToSite(result GeoResult, site *Site) {
	var country string
	for _, component := range result.AddressComponents {
		for _, component_type := range component.Types {
			switch component_type {
			case "street_number":
				site.Civic_num = component.LongName
			case "route":
				site.Streetname = component.LongName
			case "neighborhood":
				site.Town = component.LongName
			case "locality":
				site.Municipality = component.LongName
			case "administrative_area_level_1":
				site.Province = component.ShortName
			case "postal_code":
				log.Warnf("Postalcode is %v", component.LongName)
				site.PostalCode = &component.LongName
			case "country":
				country = component.LongName
			}
		}
	}
	site.Location = result.Geometry.Location
	if site.PostalCode == nil {
		site.FullAddress = site.Civic_num + " " + site.Streetname + ", " + site.Municipality + ", " + site.Province + ", " + country
	} else {
		site.FullAddress = site.Civic_num + " " + site.Streetname + ", " + site.Municipality + ", " + site.Province + " " + *site.PostalCode + ", " + country
	}
}
