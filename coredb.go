package main

import (
	"context"
	"flag"
	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"go.mongodb.org/mongo-driver/mongo/options"
	"strconv"
	"time"
)

var (
	MongoURI     = flag.String("mongouri", "mongodb://coredb01.dc1.osh.telmax.ca:27017", "MongoDB URL for telephone database")
	CoreDatabase = flag.String("coredatabase", "telmaxmb", "Core Database name")

	TicketDatabase = flag.String("ticketdatabase", "maxticket", "Database for ticketing")
	CRMDatabase    = flag.String("crmdatabase", "maxcrm", "Database for CRM")

	DBClient *mongo.Client
	CRMDB    *mongo.Database
)

func init() {
	flag.Parse()
	lvl, _ := log.ParseLevel(*LogLevel)
	log.SetLevel(lvl)
	clientOptions := options.Client().ApplyURI(*MongoURI)
	clientOptions.SetAuth(options.Credential{
		Username: "maxcoredb",
		Password: "coredbmax955TEL",
	})
	DBClient, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	log.Info("Connected to MongoDB!")
	CRMDB = DBClient.Database(*CRMDatabase)

}

// Shut down connectinos to CoreDB
func CoreDBCleanup() {
	DBClient.Disconnect(context.TODO())
}

// Log a site lookup for statistical purposes
func LogSite(site Site) {
	log.Infof("Logging lookup attempt for %v", site.FullAddress)
	entry := LogEntry{
		QueryTime:        time.Now(),
		FormattedAddress: site.FullAddress,
		Locality:         site.Municipality,
		AccessType:       site.AccessType,
		Location:         site.Location,
	}
	if site.PostalCode != nil {
		entry.PostalCode = *site.PostalCode
	}
	_, err := CRMDB.Collection("address_lookups").InsertOne(context.TODO(), entry)
	if err != nil {
		log.Errorf("Problem inserting request into log %v", err)
	}
}

// Get a list of PONs

func GetPONs(fdh string) (pons []string, err error) {
	if fdh == "087-1" {
		pons = []string{
			"brooklin-olt01-pon01",
			"brooklin-olt02-pon02",
			"brooklin-olt03-pon03",
		}
	}

	return
}

// Update the CRM database, or otherwise call back to other applications when a site is ready
func AccessReady(site Site) error {
	var err error
	if site.Lead_ID != nil {

		// Update all the leads with the correct access type
		log.Infof("Updating completion status for %v", *site.Lead_ID)
		filter := bson.D{{
			"site_id", strconv.Itoa(site.ID),
		}}

		update := bson.M{"$set": bson.M{
			"connection_type": site.AccessType,
		}}

		var result *mongo.UpdateResult
		result, err = CRMDB.Collection("leads").UpdateMany(context.Background(), filter, update)
		if err != nil {
			log.Errorf("Problem updating CRM %v", err)
		} else {
			log.Debugf("Update result is %v", result)
		}
		// Update any leads in the construction status to say access ready
		filter = bson.D{{
			"$and", []bson.M{
				bson.M{"site_id": strconv.Itoa(site.ID)},
				bson.M{"lead_status": bson.M{"$ne": "Installation"}},
				bson.M{"lead_status": bson.M{"$ne": "Activated"}},
				bson.M{"lead_status": bson.M{"$ne": "Cancelled"}},
			},
		}}

		update = bson.M{"$set": bson.M{
			"lead_status":     "accessready",
			"connection_type": site.AccessType,
		}}

		result, err = CRMDB.Collection("leads").UpdateMany(context.Background(), filter, update)
		if err != nil {
			log.Errorf("Problem updating CRM %v", err)
		} else {
			log.Debugf("Update result is %v", result)
		}

	} else {
		log.Infof("No lead ID for site %v", site.ID)
	}
	return err
}
