package main

/*
	QGIS API connects to the QGIS database and interacts with physical site objects using the demand points objects.
*/

import (
	"flag"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	//	"go.mongodb.org/mongo-driver/mongo"

	//	"context"

	"net/http"
	"os"
	"os/signal"
	"syscall"
	//	"telmax"
	"time"
)

// Set up all the CLI flags and global variables
var (
	LogLevel = flag.String("loglevel", "info", "Log Level")
	Listen   = flag.String("listen", ":5008", "HTTP API listen address:port")

	UseTLS  = flag.Bool("tls.enable", false, "Enable TLS")
	TLSCert = flag.String("tls.cert", "/etc/ssl/qgis-api.crt", "Server Certificate")
	TLSKey  = flag.String("tls.key", "/etc/ssl/private/qgis-api.key", "Server private key")

	APIKey = flag.String("apikey", "098g5467o456n78s8e5878c8ty4578uihdsrc", "API Key used for simple authentication")

	ProcessTicker = time.NewTicker(time.Minute * 10)
	DailyTicker   = time.NewTicker(time.Hour * 24)
	TZLocation    *time.Location
)

// Connect to the database
func init() {
	flag.Parse()
	lvl, _ := log.ParseLevel(*LogLevel)
	log.SetLevel(lvl)

	TZLocation, _ = time.LoadLocation("America/Toronto")

}

// Main loop - run periodic processes to facilitate ticket processing
func main() {
	// setup signal catching
	sigs := make(chan os.Signal, 1)
	// catch all signals since not explicitly listing
	signal.Notify(sigs)
	//signal.Notify(sigs,syscall.SIGQUIT)
	// method invoked upon seeing signal
	go func() {
		for {
			select {
			/*
				case <-ProcessTicker.C:
					Process()
					//				ProcessTicker.Reset(time.Minute)
				case <-DailyTicker.C:
					Daily()
					//DailyTicker.Reset(time.Hour * 24)
			*/
			case s := <-sigs:
				log.Debugf("RECEIVED SIGNAL: %s", s)
				if s == syscall.SIGQUIT || s == syscall.SIGKILL || s == syscall.SIGTERM || s == syscall.SIGINT {
					AppCleanup()
					os.Exit(1)
				} else if s == syscall.SIGHUP {
					log.Warning("Re-running process routine")
					//					Process()
					//					PreInstall()

				} else {

				}

				//else if s == syscall.SIGINFO {
				//	log.Warningf("ticketapi listening on %s TLS %t current debug level %s", *Listen, *UseTLS, *LogLevel)
				//			return
				//}

			}
		}
	}()
	// Process the first time we start right away
	Process()
	//	Daily()

	// Run the web server
	router := mux.NewRouter().StrictSlash(false)
	router.Methods("OPTIONS").HandlerFunc(HandleOptions)

	// These are all the API routes
	router.HandleFunc("/getsite/{siteID}", HandleGetSite).Methods("GET")
	router.HandleFunc("/searchsite/", HandleSearchSite).Methods("GET")
	router.HandleFunc("/getaddress/{address}", HandleGetAddress).Methods("GET")
	router.HandleFunc("/updatesitelead/{siteID}", HandleUpdateSiteLead).Methods("POST")
	router.HandleFunc("/updatesiteaccount/{siteID}", HandleUpdateSiteAccount).Methods("POST")
	router.HandleFunc("/updatesitestatus/{siteID}", HandleUpdateSiteStatus).Methods("POST")
	router.HandleFunc("/sitetest/{siteID}", HandleSiteTest).Methods("POST")
	router.HandleFunc("/droplist/{da}", HandleDropList).Methods("GET")
	router.HandleFunc("/fdhlist/{da}", HandleGetFDHs).Methods("GET")
	router.HandleFunc("/dalist/", HandleGetDAs).Methods("GET")
	router.HandleFunc("/ponlist/{fdh}", HandleGetPONs).Methods("GET")
	router.HandleFunc("/getreport/{wirecentre}", HandleGetReport).Methods("GET")

	// Start the HTTP or HTTPS server
	if *UseTLS {
		log.Warning("Listening on " + *Listen + " TLS")
		log.Fatal(http.ListenAndServeTLS(*Listen, *TLSCert, *TLSKey, router))
	} else {
		log.Warning("Listening on " + *Listen)
		log.Fatal(http.ListenAndServe(*Listen, router))
	}

}

func Process() {
	log.Debug("Running periodic process")
	//	go FixNoPostalcode(50)
}

func Daily() {

}

// Shut down the app cleanly
func AppCleanup() {
	log.Error("Stopping QGIS API Service")
	//	ProcessTicker.Stop()
	//	DailyTicker.Stop()
	//	CoreDBCleanup()
	QGISDBCleanup()

}

// Handle Options pre-flight requests
func HandleOptions(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
}

// Generate CORS headers for responses
func CORSHeaders(w http.ResponseWriter, r *http.Request) {
	headers := w.Header()
	log.Infof("Request Origin is: %v", r.Header.Get("Origin"))
	headers.Add("Access-Control-Allow-Headers", "Content-Type, Origin, Accept, token, api-key")
	headers.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
	//	headers.Add("Access-Control-Max-Age", "3600")
	headers.Add("Access-Control-Allow-Origin", "*")

}

// Check API Key Authorization
func CheckAuth(w http.ResponseWriter, r *http.Request) bool {
	if r.Header.Get("api-key") == *APIKey {
		return true
	}
	http.Error(w, "Unauthorized", http.StatusUnauthorized)
	return false
}

// Consistent respons structure - error only exists if there is an error.  Status is always "ok" or "error"
type Response struct {
	Status string      `json:"status"`
	Error  string      `json:"error,omitempty"`
	Data   interface{} `json:"data,omitempty"`
}
