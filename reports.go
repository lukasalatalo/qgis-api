package main

import (
	log "github.com/sirupsen/logrus"

	"bitbucket.org/timstpierre/telmax-common"
)

func GetWirecentreReport(wirecentre string) (fsdareport map[string]*FSDAStats, err error) {

	log.Info("Building FSDA status report for %v", wirecentre)
	filters := []Filter{
		Filter{
			Key:   "Wirecentre",
			Value: wirecentre,
		},
	}
	sites, err := SearchSites(filters, 50000)
	fsdareport = make(map[string]*FSDAStats, 48)
	for _, site := range sites {

		if fsdareport[site.FSDA] == nil {
			fsdareport[site.FSDA] = &FSDAStats{
				Name: site.FSDA,
			}
			log.Infof("New FSDA %", site.FSDA)
		}
		fsdareport[site.FSDA].Total++ // = fsdareport[site.FSDA].Total + 1
		var SiteServiced bool
		var LeadInstalled bool
		switch site.Status {
		case "Planned":
		case "Passed":
			fsdareport[site.FSDA].Passed++
		case "Serviced":
			SiteServiced = true
		case "Installed":
			fsdareport[site.FSDA].Installed++
		}
		if site.Lead_ID != nil {
			lead, leaderr := telmax.GetLead(CRMDB, *site.Lead_ID)
			if leaderr != nil {
				log.Errorf("Problem getting lead %v %v", *site.Lead_ID, leaderr)
			} else {
				if lead.CustomerReview && lead.CustomerAgreement {
					fsdareport[site.FSDA].PreSold++
				}
				if lead.PaymentDetails {
					fsdareport[site.FSDA].Payment++
				}
				if lead.DropOnly {
					fsdareport[site.FSDA].DropOnly++
				}
				if lead.LeadStatus == "Activated" {
					LeadInstalled = true
				}

			}
		}
		if SiteServiced {
			if LeadInstalled {
				fsdareport[site.FSDA].Installed++
			} else {
				fsdareport[site.FSDA].Serviced++
			}
		}

	}
	return
}

type FSDAStats struct {
	Name      string
	Total     int64
	Passed    int64
	Serviced  int64
	Installed int64
	DropOnly  int64
	PreSold   int64
	Payment   int64
}
