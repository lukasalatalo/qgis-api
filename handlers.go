package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

func HandleGetSite(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	var response Response

	id, err := strconv.ParseInt(mux.Vars(r)["siteID"], 10, 32)
	log.Infof("mux vars is %v", int(id))
	if err != nil {
		errstr := "Problem converting site ID to integer " + mux.Vars(r)["siteID"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	}
	// Clean the data!

	site, err := GetSite(int(id))
	if err != nil {
		errstr := "Problem getting site " + mux.Vars(r)["siteID"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	} else {
		response.Data = site
		response.Status = "ok"
	}
	json.NewEncoder(w).Encode(response)
}

func HandleGetFDHs(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	var response Response
	da, err := strconv.ParseInt(mux.Vars(r)["da"], 10, 32)

	log.Infof("mux vars is %v", int(da))
	if err != nil {
		errstr := "Problem converting DA to integer " + mux.Vars(r)["da"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	}
	// Clean the data!

	fdh_list, err := GetFDHList(int(da))
	if err != nil {
		errstr := "Problem getting DA " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	} else {
		response.Data = fdh_list
		response.Status = "ok"
	}
	json.NewEncoder(w).Encode(response)
}

func HandleGetDAs(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	var response Response

	da_list, err := GetDAList()
	if err != nil {
		errstr := "Problem getting DA list" + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	} else {
		response.Data = da_list
		response.Status = "ok"
	}
	json.NewEncoder(w).Encode(response)
}

func HandleGetPONs(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	var response Response
	fdh := mux.Vars(r)["fdh"]

	log.Infof("mux vars is %v", fdh)

	pon_list, err := GetPONs(fdh)
	if err != nil {
		errstr := "Problem getting pons for FDH " + fdh + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	} else {
		response.Data = pon_list
		response.Status = "ok"
	}
	json.NewEncoder(w).Encode(response)
}

func HandleGetAddress(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	reg, err := regexp.Compile(`[^a-zA-Z0-9\ \-\,]+`)
	if err != nil {
		log.Error(err)
	}
	address := reg.ReplaceAllString(mux.Vars(r)["address"], "")
	address_arr := strings.Split(address, ",")
	log.Debugf("Address components are %v", address_arr)
	var match_address string
	if len(address_arr) >= 2 {
		match_address = strings.Join(address_arr[:2], `,`)
		match_address = strings.TrimSpace(match_address)
	} else {
		match_address = address
	}
	log.Debugf("Match address is %v", match_address)
	var postal_code string
	if len(address_arr) >= 3 {
		province_postal := strings.Split(strings.TrimSpace(address_arr[2]), " ")
		log.Debugf("Province postalcode is %v length %v", province_postal, len(province_postal))
		if len(province_postal) == 3 {
			postal_code = strings.TrimSpace(strings.Join(province_postal[1:], " "))
			log.Debugf("Postal code supplied is %v", postal_code)
		}
	}
	var response Response

	sites, err := GetAddress(match_address)
	if err != nil {
		errstr := "Problem getting site matching address " + match_address + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	} else {
		if len(sites) < 1 {
			var site Site
			var georesult GeoResult

			georesult, err = Geocode(address)

			if err != nil {
				log.Errorf("Problem geocoding address %v %v", address, err)
				response.Error = "Could not geocode address for TPIA lookup"
				response.Status = "error"
			} else {
				GeoResultToSite(georesult, &site)

				var tpia_zone string
				tpia_zone, err = CheckTPIA(site.Location)
				if tpia_zone != "" {
					site.AccessType = "TPIA"
					site.WireCentre = tpia_zone
					site.Status = "TPIA Only"
				} else {
					site.AccessType = "Unserviced"
					site.Status = "Not Serviced"
				}
				LogSite(site)
				sites = append(sites, site)
				response.Status = "ok"
				response.Data = sites
			}
		} else if len(sites) == 1 {
			LogSite(sites[0])
			log.Infof("Found 1 site, returning ID %v with status %v", sites[0].ID, sites[0].Status)
			response.Status = "ok"
			response.Data = sites
		} else {
			response.Status = "ok"
			response.Data = sites
		}
	}
	json.NewEncoder(w).Encode(response)
}

func HandleSearchSite(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}

	reg, err := regexp.Compile(`[^a-zA-Z0-9\ \-\_\,]+`)
	if err != nil {
		log.Error(err)
	}

	var response Response
	requestvars := r.URL.Query()

	// Clean the data!
	log.Infof("Searching for %v", requestvars)
	var filters []Filter
	var limit int
	// Clean the data to remove special characters
	for key, value := range requestvars {
		log.Infof("request variable is %v %v", key, value)
		if key == "limit" {
			limit64, _ := strconv.ParseInt(value[0], 10, 32)
			limit = int(limit64)
		} else {
			filter := Filter{
				Key:   reg.ReplaceAllString(key, ""),
				Value: reg.ReplaceAllString(value[0], ""),
			}
			filters = append(filters, filter)
		}
	}
	if len(filters) > 0 {
		sites, err := SearchSites(filters, limit)
		response.Data = sites
		response.Status = "ok"

		if err != nil {
			errstr := "Problem getting sites " + err.Error()
			log.Error(errstr)
			response.Status = "error"
			response.Error = errstr
		} else {
			response.Status = "ok"

		}
	} else {
		response.Error = "Please specify at least one parameter"
	}

	json.NewEncoder(w).Encode(response)
}

func HandleUpdateSiteLead(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	var response Response

	id, err := strconv.ParseInt(mux.Vars(r)["siteID"], 10, 32)
	log.Infof("mux vars is %v", int(id))
	if err != nil {
		errstr := "Problem converting site ID to integer " + mux.Vars(r)["siteID"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	}
	var data struct {
		Lead string `json:"lead_id"`
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorf("Please provide lead_id in JSON %s", r)
		response.Error = "Invalid request Body"
	}
	json.Unmarshal(reqBody, &data)
	// Clean the data!
	reg, err := regexp.Compile(`[^a-zA-Z0-9]+`)
	if err != nil {
		log.Error(err)
	}
	lead := reg.ReplaceAllString(data.Lead, "")
	err = UpdateSiteLead(id, lead)
	if err != nil {
		errstr := "Problem updating site " + mux.Vars(r)["siteID"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	} else {
		response.Status = "ok"
	}
	json.NewEncoder(w).Encode(response)
}

func HandleUpdateSiteAccount(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	var response Response

	id, err := strconv.ParseInt(mux.Vars(r)["siteID"], 10, 32)
	log.Infof("mux vars is %v", int(id))
	if err != nil {
		errstr := "Problem converting site ID to integer " + mux.Vars(r)["siteID"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	}
	var data struct {
		AccountCode string
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorf("Please provide AccountCode in JSON %s", r)
		response.Error = "Invalid request Body"
	}
	json.Unmarshal(reqBody, &data)
	// Clean the data!
	reg, err := regexp.Compile(`[^a-zA-Z0-9]+`)
	if err != nil {
		log.Error(err)
	}
	account := reg.ReplaceAllString(data.AccountCode, "")
	err = UpdateSiteAccount(id, account)
	if err != nil {
		errstr := "Problem updating site " + mux.Vars(r)["siteID"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	} else {
		response.Status = "ok"
	}
	json.NewEncoder(w).Encode(response)
}

func HandleUpdateSiteStatus(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	var response Response

	id, err := strconv.ParseInt(mux.Vars(r)["siteID"], 10, 32)
	log.Infof("mux vars is %v", int(id))
	if err != nil {
		errstr := "Problem converting site ID to integer " + mux.Vars(r)["siteID"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	}
	var data struct {
		Status string
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorf("Please provide a Status in JSON %s", r)
		response.Error = "Invalid request Body"
	}
	json.Unmarshal(reqBody, &data)
	// Clean the data!
	var status string
	switch data.Status {
	case "Construction", "Passed", "Serviced", "Installed", "Not Serviced":
		status = data.Status
		break
	default:
		response.Status = "error"
		response.Error = "Please choose a valid status"
	}
	if response.Status != "error" {
		err = UpdateSiteStatus(id, status)
		if err != nil {
			errstr := "Problem updating site " + mux.Vars(r)["siteID"] + " " + err.Error()
			log.Error(errstr)
			response.Status = "error"
			response.Error = errstr
		} else {
			response.Status = "ok"
		}
	}
	json.NewEncoder(w).Encode(response)
}

func HandleSiteTest(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	if !CheckAuth(w, r) {
		return
	}
	var response Response

	id, err := strconv.ParseInt(mux.Vars(r)["siteID"], 10, 32)
	log.Infof("mux vars is %v", int(id))
	if err != nil {
		errstr := "Problem converting site ID to integer " + mux.Vars(r)["siteID"] + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	}
	var data struct {
		PON          string
		FDHPort      int64
		Terminal     string
		TerminalPort int64
		Signal       float64
		DropType     string
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorf("Please provide test data in JSON %s", r)
		response.Error = "Invalid request Body"
	}
	json.Unmarshal(reqBody, &data)
	if data.Signal < -26.0 {
		response.Error = "Signal level too low!"
	}

	if response.Status != "error" {
		err = UpdateSiteTest(id, data.PON, data.FDHPort, data.Terminal, data.TerminalPort, data.Signal)
		if err != nil {
			errstr := "Problem updating site " + mux.Vars(r)["siteID"] + " " + err.Error()
			log.Error(errstr)
			response.Status = "error"
			response.Error = errstr
		} else {
			var site Site
			site, err = GetSite(int(id))
			err = AccessReady(site)
			if err != nil {
				response.Status = "error"
				response.Error = "Problem marking lead as ready " + err.Error()
			}
			if response.Status != "error" {
				response.Status = "ok"
			} else {
				log.Errorf("Problem with signal test %v", response.Error)
			}

		}
	}
	json.NewEncoder(w).Encode(response)
}

func HandleDropList(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	//	if !CheckAuth(w, r) {
	//		return
	//	}

	var response Response

	da, err := strconv.ParseInt(mux.Vars(r)["da"], 10, 32)
	if err != nil {
		log.Errorf("Problem parsing DA %v ", err)
		response.Error = "DA must be numeric"
	} else {
		var sites []Site
		sites, err = DropSites(int(da))
		if err != nil {
			response.Error = err.Error()
			log.Errorf("Problem getting drop sites %v", err)
		} else {
			b := DropList("Drop list for DA "+mux.Vars(r)["da"], &sites)
			w.Header().Set("Content-Type", "application/pdf")
			w.Header().Set("Content-Disposition", "inline;filename=droplist.pdf")
			w.Write(b.Bytes())

		}
	}
}

func HandleGetReport(w http.ResponseWriter, r *http.Request) {
	CORSHeaders(w, r)
	/*	if !CheckAuth(w, r) {
			return
		}
	*/
	var response Response
	wirecentre := mux.Vars(r)["wirecentre"]

	log.Infof("mux vars is %v", wirecentre)

	stats, err := GetWirecentreReport(wirecentre)
	if err != nil {
		errstr := "Problem getting report for wirecentre " + wirecentre + " " + err.Error()
		log.Error(errstr)
		response.Status = "error"
		response.Error = errstr
	} else {
		response.Data = stats
		response.Status = "ok"
	}
	json.NewEncoder(w).Encode(response)
}
