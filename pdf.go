package main

import (
	"bytes"
	"flag"
	"github.com/jung-kurt/gofpdf"
	log "github.com/sirupsen/logrus"
	"strings"
)

var (
	ImagePath = flag.String("imagepath", "/usr/local/share/ticket/", "The path to image files for PDFs")
	PDF       *gofpdf.Fpdf
	TitleText *string
	//ImagePath = ""
)

func DropList(title string, sites *[]Site) *bytes.Buffer {
	pdf := gofpdf.New("P", "mm", "Letter", "")
	PDF = pdf
	log.Info("Generating PDF")
	pdf.AddPage()
	pdf.SetLineJoinStyle("bevel")
	pdf.SetHeaderFunc(PageHeader)
	TitleText = &title
	//	pdf.SetFont("Arial", "B", 16)
	/*	Logo(pdf)
		Title(pdf, title, 20)
		listx := 15.0
		listy := 40.0
	*/
	pdf.SetAutoPageBreak(true, 20)
	pdf.SetLeftMargin(15)
	//	pdf.SetXY(listx, listy)
	PageHeader()
	pdf.SetFontSize(12)
	for _, site := range *sites {
		SiteDetailLine(pdf, &site)
	}

	b := &bytes.Buffer{}
	pdf.Output(b)
	return b
}

func PageHeader() {
	Logo(PDF)
	Title(PDF, *TitleText, 20)
	PDF.SetXY(15, 40)
	PDF.SetFontStyle("B")
	height := 8.0

	PDF.CellFormat(20, height, "FSDA", "0", 0, "LB", false, 0, "")
	PDF.CellFormat(55, height, "Street Name", "0", 0, "LB", false, 0, "")
	PDF.CellFormat(25, height, "Number", "0", 0, "LB", false, 0, "")
	PDF.CellFormat(30, height, "Lead ID", "0", 0, "LB", false, 0, "")
	PDF.CellFormat(30, height, "Remarks", "0", 1, "LB", false, 0, "")
	PDF.SetFontStyle("")
}

func Logo(pdf *gofpdf.Fpdf) {
	var logoOpt gofpdf.ImageOptions
	logoOpt.ImageType = "png"
	pdf.ImageOptions(*ImagePath+"logo-blue.png", 10, 10, 50, 25, false, logoOpt, 0, "")

}

func Title(pdf *gofpdf.Fpdf, title string, y float64) {
	pdf.SetFont("Arial", "B", 14)
	title = strings.ToUpper(title)
	width := pdf.GetStringWidth(title)
	pagewidth, _ := pdf.GetPageSize()
	x := pagewidth/2 - width/2
	pdf.Text(x, y, title)
}

func SiteDetailLine(pdf *gofpdf.Fpdf, site *Site) {
	//	log.Infof("Site ID %v", site.ID)
	height := 8.0
	pdf.CellFormat(20, height, site.FSDA, "0", 0, "LB", false, 0, "")
	pdf.CellFormat(55, height, site.Streetname, "0", 0, "LB", false, 0, "")
	pdf.CellFormat(25, height, site.Civic_num, "0", 0, "LB", false, 0, "")
	if site.Lead_ID != nil {
		pdf.CellFormat(30, height, *site.Lead_ID, "0", 0, "LB", false, 0, "")
	} else {
		log.Errorf("Site %v listed but does not have lead!", site.ID)
		pdf.CellFormat(30, height, "Error - no lead!", "0", 0, "LB", false, 0, "")
	}
	pdf.CellFormat(30, height, "", "0", 1, "LB", false, 0, "")
}
