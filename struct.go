package main

import (
	"time"
)

type Site struct {
	ID                   int       // Unique ID for the site in QGIS
	DA                   string    // The distribution area name
	FSDA                 string    // The sub-distribution area
	WireCentre           string    // The central office this connection originates from
	FullAddress          string    // The full address
	Civic_num            string    // The civic number (house number)
	Unit_num             *string   // The unit number
	Streetname           string    // The street name
	Town                 string    // The town / hamlet if relevant
	Municipality         string    // The municipality name
	Province             string    // the two letter province code
	PostalCode           *string   // The postal code with the space in the middle
	Bldg_id              *string   // A unique identifier if the site is a building
	Is_mdu               bool      // Is this a MDU site
	Lead_ID              *string   // Set if a lead is associated with this point
	AccountCode          *string   // the accountcode if this is a customer.
	AccessType           string    // The type of access available, if any
	Status               string    // Current status - Planned Construction Passed Serviced Installed
	FibreSize            int       // The number of fibres installed to this point
	CircuitData          []Circuit // An array of circuits that are configured at this point
	Location             GeoPoint  // The geograhic co-ordinates
	ConstructionSchedule *string   // The expected construction time
}

type Circuit struct {
	Fibre         int     // The fibre number
	FDH           string  // The FDH cabinet this drop is fed from
	FDHPort       int     // The port on the FDH this point is connected to
	Terminal      string  // The terminal this drop is connected to
	TerminalPort  int     // The port number on the terminal
	PON           string  // The physical PON if patched
	DemarcPort    int     // The port on the demarc point
	InitialSignal float64 // The initial signal test result

}

type PON struct {
	Name       string // The text name for the PON
	WireCentre string // The Wirecentre this PON is attached to
	OLT        string // The name of the olt the PON is connected to
	OLTPort    int    // The port number on the OLT
	FDH        []struct {
		ID            string
		Feeder        int
		FeederLevel   float64
		SplitterLevel float64
		SplitterCount int
	}
}

type GeoPoint struct {
	Lat  float32 `json:"lat"` // The latitude
	Long float32 `json:"lng"` // The Longitude
}

type GeoResult struct {
	AddressComponents []AddressComponent `json:"address_components"`
	FormattedAddress  string             `json:"formatted_address"`
	Geometry          ResultGeometry     `json:"geometry"`
	PlaceID           string             `json:"place_id"`
	Types             []string           `json:"types"`
}

type ResultGeometry struct {
	Location     GeoPoint `json:"location"`
	LocationType string   `json:"location_type"`
}

type AddressComponent struct {
	LongName  string   `json:"long_name"`
	ShortName string   `json:"short_name"`
	Types     []string `json:"types"`
}

type LogEntry struct {
	QueryTime        time.Time `bson:"query_time"`
	FormattedAddress string    `bson:"formatted_address"`
	Locality         string    `bson:"locality"`
	PostalCode       string    `bson:"postal_code"`
	AccessType       string    `bson:"access_type"`
	Location         GeoPoint  `bson:"location"`
}
